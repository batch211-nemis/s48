console.log("Hello World");

//Mock Database
let posts = [];

//Post ID
let count = 1;

//Add Post
document.querySelector("#form-add-post").addEventListener("submit",(e)=>{

	e.preventDefault()

	posts.push({
		id:count,
		title:document.querySelector("#txt-title").value,
		body:document.querySelector("#text-body").value
	})
	count++
	console.log(posts);
	alert("Succesfully Added!")
	//invoke a function to show post later
	showPosts();
})

//Edit Posts
const showPosts = () =>{
	let postEntries = "";
	posts.forEach((post)=>{
		postEntries += `
			<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onClick="editPost('${post.id}')">Edit</button>
			<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

	//Edit Post Button
	const editPost = (id) =>{

		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector("#text-edit-id").value = id;
		document.querySelector("#text-edit-title").value = title;
		document.querySelector("#text-edit-body").value = body;
	} 

//Update Post
document.querySelector("#form-edit-post").addEventListener("submit",(e)=>{

	e.preventDefault();

	for (let i=0; i<posts.length; i++){
		if (posts[i].id.toString()===document.querySelector("#text-edit-id").value){
			posts[i].title = document.querySelector("#text-edit-title").value;
			posts[i].body = document.querySelector("#text-edit-body").value

			showPosts(posts);
			alert("Successfully Updated!");
			break;
		}
	}
})


//S48 ACTIVITY SOLUTION

//Delete Post


	const deletePost = (id)=>{

		index = id--;
        let del = posts.splice(index, 1);

        console.log("Removed item:", del);

        document.getElementById("div-post-entries").innerHTML = "";

		// document.querySelector("#div-post-entries").innerHTML = "";

       	console.log(posts);
       
	}

